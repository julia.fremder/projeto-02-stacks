'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
      await queryInterface.createTable("tstack", {

        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        stack:{
          allowNull: false,
          type: Sequelize.TEXT
        },
        image: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        status: {
          allowNull: false,
          type:Sequelize.BOOLEAN,
          defaultValue: true
        },
      });
  },

  down: async (queryInterface, Sequelize) => {

    return queryInterface.dropTable('tstack');
  }

};


/*
INSERT INTO `tstack`(`stack`, `image`, `status`) 
VALUES 
("back-end","https://i.ibb.co/JsmVhPk/backend.png", true),
("front-end", "https://i.ibb.co/pZrHp0m/frontend.png", true),
("mobile", "https://i.ibb.co/98z4t9B/mobile.png", true)*/