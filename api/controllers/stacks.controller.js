const {Stack, allocation} = require('../models')


const getAllStacks = async (req, res) => {

    const result = await Stack.findAll({})
    
    res.status(200).send(result.map((item) => {

        const {id, stack, image} = item;

        return {
            id,
            stack,
            image
        }

    }) || []);

}


const getStackById = async (req, res) => {
    try{
    const result = await Stack.findOne({
        where: {
            id: req.params.id
        },
        include: {
          model: allocation,
          as: 'allocation',
        },
    });

        res.status(200).send({
            id:result.id,
            stack:result.stack,
            image: result.image,
            allocation: result.allocation

      
        });
      
      } catch (error) {
        console.log(error);

    res.status(500).send({ message: 'Internal server error!!' });
      }

};


const postAllocationStack = async (req, res) => {

        try{
      
          //TODO: em qual atividade (id) será feita a inscrição?? pegar na rota.
          const {id} = req.params;
          // console.log('id da atividade: ', id);
      
          //TODO: recuperar valores de inscricao para cadastro. pegar no no body da request.
          const body = req.body;
          // console.log('body da atividade: ', body)
      
          //TODO: construir o model que será passado para o método create do sequelize que irá criar o insert no banco de dados e adicionar o registro. Esse model deve estar de acordo com a migration de criação da tabela inscrição.
          const model = {
            stack_id: id, //atividade_id é o nome do id da atividade lá na tabela de inscricao, id é o nome do id da atividade na rota.
            employee_name: body.employee_name,
            employee_email: body.employee_email,
            employee_phone: body.employee_phone,
            employee_local: body.employee_local,
            employee_state: body.employee_state
          }
      
          //TODO: mandar para o banco de dados via sequelize
          await allocation.create(model);
      
          res.status(200).send('allocation')
      
        } catch (error) {
      
          res.status(500).send({message:'Something went wrong...'})
      
        }
      
}


const deleteAllocation = async (req, res) => {

    try{
  
    //TODO: qual a inscricao deseja cancelar? pegar id na rota.
    const {idallocation} = req.params;
  
    await allocation.destroy({
      
      where: {
      
        id: idallocation,
      
      }
  
    });
  
    res.status(200).send({message: 'Deleted allocation: ' + idallocation})
  
  
    } catch (error) {
  
    res.status(500).send({message:'Allocation was not deleted.'})
  
    }
    
  
  }


module.exports = {

    getAllStacks,
    getStackById,
    postAllocationStack,
    deleteAllocation

}

