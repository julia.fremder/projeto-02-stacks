//imports
const express = require('express');
const cors = require('cors');
const router = require('./routes')


//settings
//create an express instance
const app = express();
const port = process.env.PORT || 3001;

//midleware
app.use(express.json());
app.use(cors());


//routes
//instance of express passed to router
router(app);



app.listen(port);


module.exports = app