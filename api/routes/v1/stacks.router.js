const stackController = require('../../controllers/stacks.controller')

module.exports = (router) => {

    router.route('/stack')
    .get(
        stackController.getAllStacks
    );

    router.route('/stack/:id')
    .get(
        stackController.getStackById
    );

    router.route('/stack/:id/allocation')
    .post(
        stackController.postAllocationStack
    );

    router.route('/stack/:idallocation')
    .delete(
        stackController.deleteAllocation
    );




}