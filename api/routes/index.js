const {Router} = require('express');
const {name, version} = require('../../package.json');
const app = require('../server');
const stacksRouter = require('./v1/stacks.router');


module.exports = (instanceExpress) => {

    const router = Router();

    router.route('/').get((req, res) => {

        res.send({name, version});

    })

    stacksRouter(router);

    instanceExpress.use('/v1', router);

};