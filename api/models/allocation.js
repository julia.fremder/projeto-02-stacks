module.exports = (sequelize, DataTypes) => {
    const allocation = sequelize.define(
        "allocation", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false

            },
            employee_name: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            employee_email: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            employee_phone: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            employee_local: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            employee_state: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            
        },
        {
        undescored: true,
        paranoid: true,
        timestamps: false,
        tableName:"tallocation"
    })

    allocation.associate = function (models) {
        allocation.belongsTo(models.Stack, {
          foreignKey: 'stack_id',
          as: 'stacks'
        });
      };
    

    return allocation;
}