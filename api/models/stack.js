module.exports = (sequelize, DataTypes) => {

    const Stack = sequelize.define(
        "Stack", {

            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
                allowNull: false

            },
            stack: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            image: {
                type: DataTypes.TEXT,
                allowNull: false,
            }},
            {
                undescored: true,
                paranoid: true,
                timestamps: false,
                tableName: "tstack"
            })

            Stack.associate = function(models) {
                Stack.hasMany(models.allocation, {
                    foreignKey: 'stack_id',
                    as: "allocation"
                });
            };

    return Stack;

}